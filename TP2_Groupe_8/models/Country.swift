//
//  Country.swift
//  TP2_Groupe_8
//
//  Created by mbds on 20/03/2021.
//

import Foundation

struct Country{
    var isoCode: String
    var name: String
    var continent: String
}
