//
//  Countries.swift
//  TP2_Groupe_8
//
//  Created by mbds on 20/03/2021.
//

import Foundation

let countries = [
    Country(isoCode: "at", name: "Austria", continent: "Europe"),
    Country(isoCode: "be", name: "Belgium", continent: "Europe"),
    Country(isoCode: "de", name: "Germany",continent: "Europe"),
    Country(isoCode: "el", name: "Greece", continent: "Europe"),
    Country(isoCode: "fr", name: "France",continent: "Europe"),
    Country(isoCode: "mg", name: "Madagascar", continent: "Afrique"),
    Country(isoCode: "br", name: "Brésil", continent: "Amerique")
]


